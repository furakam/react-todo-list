import { useLocalStorage } from "@mantine/hooks";
import { Task } from "../interfaces/task.interface";
import { v4 as uuidv4 } from "uuid";

export default function useTask() {
  const [tasks, setTasks] = useLocalStorage<Task[]>({
    key: 'tasks',
    defaultValue: [],
  })

  const addTask = (task: string) => {
    const tasktoRegister: Task = {
      id: uuidv4(),
      task,
      done: false
    }
    setTasks([...tasks, tasktoRegister])
  }

  const removeTask = (id: string) => {
    const taskFiltered = tasks.filter(t => t.id !== id)
    setTasks(taskFiltered)
  }

  const toggleDoneTask = (id: string) => {
    const newTasks = tasks.map(t => {
      if (t.id === id) {
        t.done = !t.done
      }
      return t
    })
    setTasks(newTasks)
  }

  return {
    tasks,
    addTask,
    removeTask,
    toggleDoneTask
  }
}