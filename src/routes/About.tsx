import GitLabLogo from '../assets/gitlab.svg'

const About: React.FC = () => {
  return (
    <div className='mt-10'>
      <h2 className="text-center text-4xl font-light">To do app</h2>
      <div className="my-28 flex flex-col gap-10 items-center">
        <p className="text-gray-700 text-center max-w-lg">
          Hi, I'm Frank 😄. I created this application because I had never created a task app before although I have some time developing in React 🙃 . Well anyway it was a good experience and I hope I didn't complicate it too much 😅.
        </p>
        <div className="flex flex-col gap-5 items-center group">
          <img src={GitLabLogo} alt="GitLab" className='w-14' />
          <a href="https://gitlab.com/furakam/react-todo-list" className="text-center text-indigo-600 group-hover:underline" target="_blank">Click to go to the <span className='text-orange-500 group-hover:underline'>GitLab</span> repository</a>
        </div>
      </div>
    </div>
  )
}

export default About