import { Navbar } from "../components";
import { Outlet } from "react-router-dom";

export default function Root() {
  return (
    <>
      <Navbar />
      <main className="px-3 md:px-0 py-4">
        <Outlet />
      </main>
    </>
  )
}