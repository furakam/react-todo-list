import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import { clsx } from "clsx";
import { Task } from "../../../interfaces/task.interface";
import useTask from "../../../hooks/useTask";

interface TaskProps {
  task: Task
}

const Task: React.FC<TaskProps> = ({ task }) => {

  const {toggleDoneTask, removeTask} = useTask()

  return (
    <div className="flex gap-5 items-center bg-white rounded-xl p-4 shadow-md">
      <p className={clsx('w-full', task.done ? 'line-through text-gray-300' : 'text-zinc-500')}>{task.task}</p>
      <div className="flex gap-2">
        <button className={clsx(
          `border rounded-full p-1 cursor-pointer w-6 h-6`,
          task.done ? 'bg-green-400 border-green-400' : 'bg-zinc-100  border-gray-400'
        )}
          onClick={() => toggleDoneTask(task.id)}
        >
          <AiOutlineCheck className={clsx('text-zinc-100', task.done && 'text-white')} />
        </button>
        <button className="rounded-full w-6 h-6 bg-red-500" onClick={() => removeTask(task.id)}>
          <AiOutlineClose className="m-auto text-white" />
        </button>
      </div>
    </div>
  );
}
export default Task;