import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import taskValidationSchema from "../../../validations/task.validation";
import useTask from "../../../hooks/useTask";

interface TaskForm {
  task: string
}

interface TaskFormProps {
  closeModal: () => void
}

export default function TaskForm({ closeModal }: TaskFormProps) {

  const { addTask } = useTask()

  const initialValues: TaskForm = {
    task: ''
  }

  const onSubmit = (values: TaskForm, helpers: FormikHelpers<TaskForm>) => {
    addTask(values.task)
    helpers.setSubmitting(false)
    closeModal()
  }

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={taskValidationSchema}
      >
        {
          ({ isSubmitting }) => (
            <Form className="flex flex-col gap-4">
              <div>
                <Field name="task" type="text" className="w-full p-3 outline-none text-[17px] text-zinc-600 rounded" autoComplete='off' />
                <ErrorMessage component={'p'} name="task" className="text-red-500 text-center mt-2" />
              </div>
              <button type="submit" disabled={isSubmitting} className="w-full h-10 text-white bg-indigo-500 hover:bg-indigo-600 col-span-2 rounded">Add</button>
            </Form>
          )
        }
      </Formik>
    </>
  )
}