import { Modal } from "../../components"
import useModal from "../../hooks/useModal"
import useTask from "../../hooks/useTask";
import { Task, TaskForm } from "./components"
import { AiOutlinePlus } from "react-icons/ai";

const Home: React.FC = () => {

  const { tasks } = useTask()

  const { isOpen, openModal, closeModal } = useModal()

  return (
    <>
      <div className="space-y-4 mx-auto">
        <h2 className="text-center text-4xl font-extralight text-gray-600">All Tasks</h2>
        <div className="max-w-2xl mx-auto flex flex-col gap-4 pt-4 pb-44">
          {
            tasks.length > 0 ? tasks.map((task) => (
              <Task key={task.id} task={task} />
            ))
              :
              <div className="h-96 flex items-center justify-center">
                <p className="text-center text-gray-400 text-xl font-light">Create a task</p>
              </div>
          }
        </div>
      </div>
      <div className="fixed left-0 bottom-0 w-full h-36 flex blur-bg backdrop-blur-[1px]">
        {
          !isOpen ?
            <button className="bg-indigo-700 text-white p-5 rounded-full m-auto" onClick={openModal}>
              <AiOutlinePlus size={30} />
            </button>
            : null
        }
      </div>
      <Modal isOpen={isOpen}>
        <div className="flex flex-col gap-4">
          <span className="text-3xl text-center text-slate-700 my-4">Add a task</span>
          <div>
            <TaskForm closeModal={closeModal} />
          </div>
          <button className="w-full h-10 text-white bg-red-500 hover:bg-red-600 rounded" onClick={closeModal}>Close</button>
        </div>
      </Modal>
    </>
  )
}

export default Home