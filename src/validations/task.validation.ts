import * as Yup from "yup"


const taskValidationSchema = Yup.object().shape({
  task: Yup.string().required("The task is required")
})

export default taskValidationSchema