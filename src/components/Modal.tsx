import React, { useEffect } from "react";
import { createPortal } from "react-dom";
import { clsx } from "clsx";

interface ModalProps {
  children: React.ReactNode
  isOpen: boolean
}

const Modal: React.FC<ModalProps> = ({ children, isOpen }) => {

  useEffect(() => {
    if (isOpen) {
      document.querySelector('body')?.classList.add('no-scroll')
    }else{
      document.querySelector('body')?.classList.remove('no-scroll')
    }
  },[isOpen])

  return (
    <>
      {
        isOpen ?
          createPortal(
            (
              <div className={clsx('fixed flex top-0 w-screen min-h-screen bg-neutral-800 bg-opacity-50 p-3 animate__animated',isOpen && 'animate__fadeIn')}>
                <div className="bg-zinc-200 p-4 m-auto w-full sm:max-w-lg rounded-md">
                  {children}
                </div>
              </div>
            ),
            document.querySelector('#modal-root') as Element
          )
          : null
      }
    </>
  );
}
export default Modal;