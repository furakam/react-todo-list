import { NavLink } from "react-router-dom";


const Navbar: React.FC = () => {
  const navlinks = [
    {
      href: '/',
      name: 'Home'
    },
    {
      href: '/about',
      name: 'About'
    }
  ]
  return (
    <header className="bg-indigo-600 py-4">
      <nav className="container flex justify-between px-5 mx-auto">
        <h1 className="text-white text-xl font-light">React ToDo</h1>
        <ul className="flex gap-3">
          {
            navlinks.map((navlink) => (
              <li key={navlink.name}>
                <NavLink className={`nav-link ${(isActive: boolean) => (isActive && 'active')}`} to={navlink.href}>{navlink.name}</NavLink>
              </li>
            ))
          }
        </ul>
      </nav>
    </header>
  );
}
export default Navbar;